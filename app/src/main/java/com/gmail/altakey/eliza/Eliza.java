/**
 * Eliza.java: crude port of Eliza.pm
 * Copyright (C) 2014 Takahiro Yoshimura <altakey@gmail.com>
 *
 * Original copyright:
 * Copyright (c) 1997-2003 John Nolan. All rights reserved.
 * This program is free software.  You may modify and/or
 * distribute it under the same terms as Perl itself.
 * This copyright notice must remain attached to the file.
 *
 * You can run this file through either pod2man or pod2html
 * to produce pretty documentation in manual or html file format
 * (these utilities are part of the Perl 5 distribution).
 *
 * POD documentation is distributed throughout the actual code
 * so that it also functions as comments.
 */
package com.gmail.altakey.eliza;

import android.text.TextUtils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Random;
import java.security.SecureRandom;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Deque;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Arrays;
import java.io.File;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.NoSuchElementException;

public class Eliza {
    private Random mRandom = new SecureRandom();

	private String mName = "Eliza";
    private String mScriptFile;
    private final boolean mMemoryOn = true;
    private final Map<String, List<String>> mReasmbListForMemory = new HashMap<>();
    private final Map<String, List<String>> mReasmbList = new HashMap<>();
    private final Deque<String> mMemory = new LinkedList<>();
    private final int mMaxMemorySize = 5;
    private final float mLikelihoodOfUsingMemory = 1.0f;
    private String mTransformText;

    private Map<String, List<String>> mDecompList = new HashMap<>();
    private Set<String> mQuit = new HashSet<>();
    private List<String> mInitial = new LinkedList<>();
    private List<String> mFinal = new LinkedList<>();
    private Map<String, String> mPre = new HashMap<>();
    private Map<String, String> mPost = new HashMap<>();
    private Map<String, List<String>> mSynon = new HashMap<>();
    private Map<String, Integer> mKeyRanks = new HashMap<>();

    public Eliza(final String name, final InputStream script) throws IOException {
        mName = name;
        parseScriptData(script);
    }

    public String getName() {
        return mName;
    }

    public String say(final String type) {
        switch (type) {
        case "initial":
            return choose(mInitial);
        case "final":
            return choose(mFinal);
        default:
            throw new IllegalArgumentException(String.format("unknown type: %s", type));
        }
    }

    public String respondOn(final String userInput) {
        return transform(userInput, false);
    }

    private String choose(final List<String> choices) {
        return choices.get(mRandom.nextInt(choices.size()));
    }

    public static void main(String[] args) {
        try {
            final Eliza personality = new Eliza("Eliza", new FileInputStream("Eliza.script"));

            String botPrompt;
            String userPrompt;
            boolean promptsOn = true;
            String userInput = "";
            String previousUserInput;
            String reply;

            botPrompt = String.format("%s:\t", personality.getName());
            userPrompt = String.format("you:\t");

            if (promptsOn) {
                System.out.print(botPrompt);
            }

            System.out.println(personality.say("initial"));

            while (true) {
                if (promptsOn) {
                    System.out.print(userPrompt);
                }

                previousUserInput = userInput;
                userInput = new Scanner(new InputStreamReader(System.in)).useDelimiter("\n").next();

                if (personality.testQuit(userInput)) {
                    reply = personality.say("final");
                    if (promptsOn) {
                        System.out.print(botPrompt);
                    }
                    System.out.println(reply);
                    break;
                }

                reply = personality.respondOn(userInput);

                if (promptsOn) {
                    System.out.print(botPrompt);
                }
                System.out.println(reply);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String preprocess(final String string) {
        return trim(applyReplaceTable(string, mPre));
    }

    private String postprocess(final String string) {
        return trim(applyReplaceTable(string, mPost));
    }

    public boolean testQuit(final String string) {
        return looksLike(string, mQuit);
    }

    private static boolean looksLike(final String s1, final String s2) {
        return Pattern.compile(String.format("\\b%s\\b", s2), Pattern.CASE_INSENSITIVE).matcher(s1).find();
    }

    private static boolean looksLike(final String needle, final Set<String> haystack) {
        for (String s : haystack) {
            if (looksLike(needle, s)) {
                return true;
            }
        }
        return false;
    }

    private static String applyReplaceTable(final String target, final Map<String, String> table) {
        String stage = target;
        for (Map.Entry<String, String> e : table.entrySet()) {
            stage = Pattern.compile(e.getKey(), Pattern.CASE_INSENSITIVE).matcher(stage).replaceAll(e.getValue());
        }
        return target;
    }

    private static String trim(final String target) {
        return target.replaceAll("  +", " ");
    }

    private String transform(String string, final boolean useMemory) {
        int rank = -2;
        String reasmb = "";
        String goto_ = "";
        String reasmbKey = "";

        string = preprocess(string).replaceAll("[?!,]", ".").replaceAll("but", ".");

        final List<String> string_parts = Arrays.asList(string.split("\\."));
    STRING_PARTS:
        for (String string_part : string_parts) {
            KEYWORD:
                for (String keyword : mDecompList.keySet()) {
                    boolean redo = false;
                    do {
                        final int keyRank = mKeyRanks.get(keyword);
                        if ((looksLike(string_part, keyword) || keyword.equals(goto_)) && rank < keyRank) {
                            rank = keyRank;
                        DECOMP:
                            for (String decomp : mDecompList.get(keyword)) {
                                String this_decomp = Pattern.compile("\\s*\\*\\s*").matcher(decomp).replaceAll("\\(\\.\\*\\)");

                                if (this_decomp.contains("@")) {
                                    final String synonym_index = Pattern.compile(".*@(\\w*).*", Pattern.CASE_INSENSITIVE).matcher(this_decomp).replaceFirst("$1");
                                    final String synonyms = TextUtils.join("|", mSynon.get(synonym_index));
                                    this_decomp = Pattern.compile(String.format("(.*)@%s(.*)", synonym_index)).matcher(this_decomp).replaceAll(String.format("$1(%s|%s)$2", synonym_index, synonyms));
                                }

                                final Matcher matcher = Pattern.compile(String.format("%s()()()()()()()()()()", this_decomp), Pattern.CASE_INSENSITIVE).matcher(string_part);
                                if (matcher.find()) {
                                    final List<String> decomp_matches = Arrays.asList(new String[] {
                                            "0",
                                            matcher.group(1),
                                            matcher.group(2),
                                            matcher.group(3),
                                            matcher.group(4),
                                            matcher.group(5),
                                            matcher.group(6),
                                            matcher.group(7),
                                            matcher.group(8),
                                            matcher.group(9),
                                        });
                                    reasmbKey = TextUtils.join(",", new String[]{keyword, decomp});

                                    List<String> these_reasmbs;
                                    if (useMemory) {
                                        these_reasmbs = mReasmbListForMemory.get(reasmbKey);
                                    } else {
                                        these_reasmbs = mReasmbList.get(reasmbKey);
                                    }
                                    reasmb = choose(these_reasmbs);

                                    final Matcher gotoMatcher = Pattern.compile("^goto\\s(\\w*).*", Pattern.CASE_INSENSITIVE).matcher(reasmb);
                                    if (gotoMatcher.find()) {
                                        goto_ = keyword = gotoMatcher.group(1);
                                        rank = -2;
                                        redo = true;
                                        break DECOMP;
                                    }

                                    for (int i=1; i<decomp_matches.size(); ++i) {
                                        decomp_matches.set(i, postprocess(decomp_matches.get(i)).replaceFirst("([,;?!]|\\.*)$", ""));
                                        reasmb = reasmb.replaceAll(String.format("\\(%d\\)", i), decomp_matches.get(i));
                                    }

                                    continue KEYWORD;
                                }
                            }
                        }
                        if (redo) {
                            redo = false;
                            continue;
                        }
                    } while (false);
                }
        }

        if ("".equals(reasmb)) {
            if (mMemory.size() > 0 && shouldUseMemory()) {
                final String m = mMemory.removeFirst();
                reasmb = transform(m, true);
            } else {
                reasmb = transform("xnone", false);
            }
        } else if (mMemoryOn) {
            if (mReasmbListForMemory.containsKey(reasmbKey) && !useMemory) {
                mMemory.addLast(string);
            }
            if (mMemory.size() > mMaxMemorySize) {
                mMemory.removeFirst();
            }
        }

        reasmb = trimAroundQuestionMark(trim(reasmb));
        mTransformText = reasmb;
        return reasmb;
    }

    private boolean shouldUseMemory() {
        return mRandom.nextDouble() >= (1 - mLikelihoodOfUsingMemory);
    }

    private static String trimAroundQuestionMark(final String target) {
        return target.replaceAll("  +", " ");
    }

    private void parseScriptData(final InputStream script) throws IOException {
        final Scanner scanner = new Scanner(script, "UTF-8");
        try {
            String thiskey = "";
            String thisdecomp = "";

            while (true) {
                final String line = scanner.nextLine();
                if (Pattern.compile("^\\s*#|^\\s*$").matcher(line).matches()) {
                    continue;
                }
                Matcher m = Pattern.compile("^\\s*(\\S*)\\s*:\\s*(.*)\\s*$").matcher(line);
                if (m.matches()) {
                    final String entryType = m.group(1);
                    final String entry = m.group(2);
                    String[] kv;

                    switch (entryType) {
                    case "quit":
                        mQuit.add(entry);
                        break;
                    case "initial":
                        mInitial.add(entry);
                        break;
                    case "final":
                        mFinal.add(entry);
                        break;
                    case "decomp":
                        if ("".equals(thiskey)) {
                            throw new IllegalArgumentException("error parsing script: decomposition rule with no keyword");
                        }
                        thisdecomp = TextUtils.join(",", new String[] {thiskey, entry});
                        if (mDecompList.containsKey(thiskey)) {
                            mDecompList.get(thiskey).add(entry);
                        } else {
                            mDecompList.put(thiskey, asArrayList(new String[] {entry}));
                        }
                        break;
                    case "reasmb":
                        if ("".equals(thisdecomp)) {
                            throw new IllegalArgumentException("error parsing script: reassembly rule with no decomposition rule");
                        }
                        if (mReasmbList.containsKey(thisdecomp)) {
                            mReasmbList.get(thisdecomp).add(entry);
                        } else {
                            mReasmbList.put(thisdecomp, asArrayList(new String[] {entry}));
                        }
                        break;
                    case "reasm_for_memory":
                        if ("".equals(thisdecomp)) {
                            throw new IllegalArgumentException("error parsing script: reassembly rule with no decomposition rule");
                        }
                        if (mReasmbListForMemory.containsKey(thisdecomp)) {
                            mReasmbListForMemory.get(thisdecomp).add(entry);
                        } else {
                            mReasmbListForMemory.put(thisdecomp, asArrayList(new String[] {entry}));
                        }
                        break;
                    case "pre":
                        kv = parseAsKeyValue(entry);
                        mPre.put(kv[0], kv[1]);
                        break;
                    case "post":
                        kv = parseAsKeyValue(entry);
                        mPost.put(kv[0], kv[1]);
                        break;
                    case "synon":
                        kv = parseAsKeyValue(entry);
                        mSynon.put(kv[0], Arrays.asList(kv[1].split("\\ ")));
                        break;
                    case "key":
                        kv = parseAsKeyValue(entry);
                        thiskey = kv[0];
                        thisdecomp = "";
                        mKeyRanks.put(thiskey, Integer.valueOf(kv[1]));
                        break;
                    }
                }
            }
        } catch (NoSuchElementException e) {
        }
    }

    private void dump() {
        for (Map.Entry<String, List<String>> e : mReasmbList.entrySet()) {
            for (String s : e.getValue()) {
                System.out.println(String.format("reasmblist: %s -> %s", e.getKey(), s));
            }
        }

        for (Map.Entry<String, List<String>> e : mReasmbListForMemory.entrySet()) {
            for (String s : e.getValue()) {
                System.out.println(String.format("reasmblist (memory): %s -> %s", e.getKey(), s));
            }
        }

        for (Map.Entry<String, List<String>> e : mDecompList.entrySet()) {
            for (String s : e.getValue()) {
                System.out.println(String.format("decomp: %s -> %s", e.getKey(), s));
            }
        }

        for (Map.Entry<String, List<String>> e : mSynon.entrySet()) {
            for (String s : e.getValue()) {
                System.out.println(String.format("synon: %s -> %s", e.getKey(), s));
            }
        }
    }

    private static String[] parseAsKeyValue(final String entry) {
        final Matcher kvm = Pattern.compile("^\\s*(\\S*)\\s*(.*)").matcher(entry);
        kvm.matches();
        return new String[] { kvm.group(1), kvm.group(2) };
    }

    private static <T> ArrayList<T> asArrayList(final T[] array) {
        return new ArrayList<T>(Arrays.asList(array));
    }
}
