package com.gmail.altakey.eliza;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;


public class PersonalityService extends Service
{
    public static final String ACTION_USER_TALK = "talk_user";
    public static final String ACTION_ELIZA_TALK = "talk_eliza";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_FINAL = "final";

    private Eliza mPersonality;
    private BroadcastReceiver mReceiver = new TalkReceiver();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(ACTION_USER_TALK));

        try {
            if (mPersonality == null) {
                mPersonality = new Eliza("Eliza", getResources().openRawResource(R.raw.eliza_script));
                talk(this, ACTION_ELIZA_TALK, mPersonality.say("initial"));
            }
            return START_STICKY;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        if (mPersonality != null) {
            mPersonality = null;
        }
    }

    public static void talk(final Context c, final String action, final String msg) {
        talk(c, action, msg, false);
    }

    public static void talk(final Context c, final String action, final String msg, boolean final_) {
        final Intent intent = new Intent(action);
        intent.putExtra(KEY_MESSAGE, msg);
        intent.putExtra(KEY_FINAL, final_);
        LocalBroadcastManager.getInstance(c).sendBroadcast(intent);
    }

    private class TalkReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String msg = intent.getStringExtra(KEY_MESSAGE);
            if (mPersonality.testQuit(msg)) {
                talk(context, ACTION_ELIZA_TALK, mPersonality.say("final"), true);
            } else {
                talk(context, ACTION_ELIZA_TALK, mPersonality.respondOn(msg));
            }
        }
    }
}
