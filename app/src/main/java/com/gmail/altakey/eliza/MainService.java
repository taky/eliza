package com.gmail.altakey.eliza;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.glass.timeline.LiveCard;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class MainService extends Service
{
    private BroadcastReceiver mReceiver = new TalkReceiver();

    private ElizaCard mCard;
    private Talker mTalker;
    private SpeechListener mListener;
    private PowerManager.WakeLock mWakeLock;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        final PowerManager pm = (PowerManager)getSystemService(POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "eliza_main");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mWakeLock.acquire();

        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(PersonalityService.ACTION_ELIZA_TALK));

        if (mCard == null) {
            mCard = new ElizaCard(this);
            mCard.attach(this);
            mCard.setAction(PendingIntent.getActivity(this, 1, new Intent(this, ElizaMenuActivity.class), 0));
            mCard.publish(LiveCard.PublishMode.REVEAL);
        }

        if (mTalker == null) {
            mTalker = new Talker();
            mTalker.init();
        }

        final ConnectivityManager cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        final NetworkInfo network = cm.getActiveNetworkInfo();
        if (network != null && network.isConnected()) {
            if (mListener == null) {
                mListener = new SpeechListener();
                mListener.init();
            }

            final Intent service = new Intent(this, PersonalityService.class);
            startService(service);
            return START_STICKY;
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mTalker.talk("oh sorry, i need an active network connection to be functional.  later.", true);
                }
            }, 1000);
            return START_STICKY;
        }
    }

    @Override
    public void onDestroy() {
        final Intent service = new Intent(this, PersonalityService.class);
        stopService(service);

        if (mListener != null) {
            mListener.shutdown();
            mListener = null;
        }

        if (mTalker != null) {
            mTalker.shutdown();
            mTalker = null;
        }

        if (mCard != null) {
            mCard.unpublish();
            mCard = null;
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        mWakeLock.release();
    }

    private class TalkReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String msg = intent.getStringExtra(PersonalityService.KEY_MESSAGE);
            final boolean final_ = intent.getBooleanExtra(PersonalityService.KEY_FINAL, false);
            mTalker.talk(msg, final_);
        }
    }

    private class Talker implements TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener {
        private final String FAREWELL = "final_";
        private TextToSpeech mmTTS;

        public Talker() {
        }

        public void init() {
            if (mmTTS == null) {
                mmTTS = new TextToSpeech(MainService.this, this);
                mmTTS.setOnUtteranceCompletedListener(this);
                mmTTS.setLanguage(Locale.US);
            }
        }

        public void shutdown() {
            if (mmTTS != null) {
                mmTTS.shutdown();
                mmTTS = null;
            }
        }

        public void talk(final String msg, boolean final_) {
            final HashMap<String, String> params = new HashMap<>();
            params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, "STREAM_NOTIFICATION");
            if (final_) {
                params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, FAREWELL);
            }
            mmTTS.speak(msg, TextToSpeech.QUEUE_FLUSH, params);
        }

        @Override
        public void onInit(int status) {

        }

        @Override
        public void onUtteranceCompleted(String utteranceId) {
            if (FAREWELL.equals(utteranceId)) {
                stopSelf();
            }
        }
    }

    private class SpeechListener implements RecognitionListener {
        private SpeechRecognizer mmSTT;
        private Intent mmParams;

        public SpeechListener() {
        }

        public void init() {
            if (mmSTT == null) {
                mmSTT = SpeechRecognizer.createSpeechRecognizer(MainService.this);
                mmSTT.setRecognitionListener(this);
                start();
            }
        }

        private void start() {
            mmParams = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            mmParams.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
            mmParams.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            mmParams.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
            restart();
        }

        private void restart() {
            mmSTT.startListening(mmParams);
        }

        public void shutdown() {
            mmSTT.stopListening();
            mmSTT.destroy();
        }

        @Override
        public void onReadyForSpeech(Bundle params) {
            mCard.setRecognitionStatus("(active)");
        }

        @Override
        public void onEvent(int eventType, Bundle params) {

        }

        @Override
        public void onPartialResults(Bundle partialResults) {
            final String msg = TextUtils.join(" ", partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION));
            mCard.setRecognitionStatus(String.format("%s ...", msg));
        }

        @Override
        public void onResults(Bundle results) {
            final String msg = TextUtils.join(" ", results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION));
            mCard.setRecognitionStatus(String.format("%s.", msg));
            PersonalityService.talk(MainService.this, PersonalityService.ACTION_USER_TALK, msg);
            restart();
        }

        @Override
        public void onError(int error) {
            Log.d("MS", String.format("error: %d", error));
            if (error != 8) {
                PersonalityService.talk(MainService.this, PersonalityService.ACTION_USER_TALK, "...");
                restart();
            }
        }

        @Override
        public void onEndOfSpeech() {
            Log.d("MS", "speech completed");
            restart();
        }

        @Override
        public void onBufferReceived(byte[] buffer) {

        }

        @Override
        public void onRmsChanged(float rmsdB) {

        }

        @Override
        public void onBeginningOfSpeech() {
            mCard.setRecognitionStatus("...");
        }
    }
}
