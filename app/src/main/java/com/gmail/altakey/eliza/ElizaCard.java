package com.gmail.altakey.eliza;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.google.android.glass.timeline.LiveCard;


public class ElizaCard extends LiveCard
{
    private RemoteViews mViews;
    private static String CARD_TAG = "com.gmail.altakey.bette.card";

    public ElizaCard(Context context) {
        super(context, CARD_TAG);
        init(context);
    }

    private void init(final Context context) {
        mViews = new RemoteViews(context.getPackageName(), R.layout.card_main);
        setViews(mViews);
    }

    public void setRecognitionStatus(final String status) {
        mViews.setTextViewText(R.id.status, status);
        setViews(mViews);
    }
}
