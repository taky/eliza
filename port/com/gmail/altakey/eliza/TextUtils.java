package com.gmail.altakey.eliza;

import java.util.List;
import java.util.Arrays;

public class TextUtils {
    public static String join(final String glue, final List<String> iter) {
        final StringBuilder sb = new StringBuilder();
        if (iter.size() > 0) {
            sb.append(iter.get(0));
            if (iter.size() > 1) {
                for (int i=1; i<iter.size(); ++i) {
                    sb.append(glue);
                    sb.append(iter.get(i));
                }
            }
        }
        return sb.toString();
    }

    public static String join(final String glue, final String[] iter) {
        return join(glue, Arrays.asList(iter));
    }
}
